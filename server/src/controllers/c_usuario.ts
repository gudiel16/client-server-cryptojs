import { Request, Response } from 'express';

import {encrypt_cryptojs} from '../cifrar/encrypt_cryptojs';
import {decrypt_cryptojs} from '../cifrar/decrypt_cryptojs';

//import pool from '../database';

class C_usuario{

    public async encrypt(req:Request,res:Response) {

        const {nick, pass} = req.body;

        let cif = await encrypt_cryptojs.encrypt(pass,nick);

        res.status(200).json({pass:cif,nick:nick});
    }

    public async decrypt(req:Request,res:Response) {

        const {data, nick} = req.body;

        let dec = await decrypt_cryptojs.decrypt(data,nick);

        res.status(200).send(dec);
    }
    
    
}

export const c_usuario = new C_usuario();