import { Router } from 'express';

import {c_usuario} from '../controllers/c_usuario';

class IndexRoutes{
    public router: Router = Router();

    constructor(){
        this.config();
    }

    config(): void {
        this.router.post('/login', c_usuario.encrypt);
        this.router.post('/decrypt', c_usuario.decrypt);
    }
}

const indexRoutes = new IndexRoutes();
export default indexRoutes.router;