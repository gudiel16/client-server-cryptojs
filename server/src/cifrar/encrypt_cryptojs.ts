import * as CryptoJS from 'crypto-js';

//import pool from '../database';

class Encrypt_cryptojs{


    public async encrypt(data: string, password: string): Promise<string> {

        const salt = CryptoJS.lib.WordArray.create([1123456789,1123456789,1123456789,1123456789]);
        console.log(salt);
        // generate password based key
        const key = CryptoJS.PBKDF2(password, salt, {
            keySize: 8,
            iterations: 1024,
        });

        // encrypt using random IV
        const iv = CryptoJS.lib.WordArray.create([1123456789,1123456789,1123456789,1123456789]);
        const encrypted = CryptoJS.AES.encrypt(data, key, {
            iv: iv,
            padding: CryptoJS.pad.Pkcs7,
            mode: CryptoJS.mode.CBC,
        });

        // salt (16 bytes) + iv (16 bytes)
        // prepend them to the ciphertext for use in decryption
        return salt.toString() + iv.toString() + encrypted.toString();
    }   
    
}

export const encrypt_cryptojs = new Encrypt_cryptojs();