import * as CryptoJS from 'crypto-js';

//import pool from '../database';

class Decrypt_cryptojs{

    public async decrypt(data: string, nick: string): Promise<string> {

        const salt = CryptoJS.enc.Hex.parse(data.substr(0, 32));
        const iv = CryptoJS.enc.Hex.parse(data.substr(32, 32));
        const encrypted = data.substring(64);

        // generate password based key
        const key = CryptoJS.PBKDF2(nick, salt, {
            keySize: 8,
            iterations: 1024,
        });

        // decrypt using custom IV
        const decrypted = CryptoJS.AES.decrypt(encrypted, key, {
            iv: iv,
            padding: CryptoJS.pad.Pkcs7,
            mode: CryptoJS.mode.CBC,
        });

        return decrypted.toString(CryptoJS.enc.Utf8);
    }    
    
}

export const decrypt_cryptojs = new Decrypt_cryptojs();