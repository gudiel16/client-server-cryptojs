"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const morgan_1 = __importDefault(require("morgan"));
const cors_1 = __importDefault(require("cors"));
const indexRoutes_1 = __importDefault(require("./routes/indexRoutes"));
class Server {
    //se ejecuta al instanciar la clase, y devolvera objeto tipo express
    constructor() {
        this.app = express_1.default();
        this.config();
        this.routes();
    }
    //encargado de configurar la variable app
    config() {
        this.app.set('port', process.env.PORT || 3000);
        this.app.use(morgan_1.default('dev'));
        this.app.use(cors_1.default());
        this.app.use(express_1.default.json({ limit: '50mb' }));
        this.app.use(express_1.default.urlencoded({ extended: false })); // para enviar desde un formulario html
    }
    //para definir de app las rutas de nuestro servidor
    routes() {
        this.app.use('/', indexRoutes_1.default);
    }
    //para poder inicializar el servidor
    start() {
        const serverWeb = this.app.listen(this.app.get('port'), "0.0.0.0", () => {
            console.log("Ejecutando Server en port", this.app.get('port'));
        });
    }
}
const server = new Server();
server.start();
