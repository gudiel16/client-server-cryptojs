"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    Object.defineProperty(o, k2, { enumerable: true, get: function() { return m[k]; } });
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __setModuleDefault = (this && this.__setModuleDefault) || (Object.create ? (function(o, v) {
    Object.defineProperty(o, "default", { enumerable: true, value: v });
}) : function(o, v) {
    o["default"] = v;
});
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (k !== "default" && Object.prototype.hasOwnProperty.call(mod, k)) __createBinding(result, mod, k);
    __setModuleDefault(result, mod);
    return result;
};
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.encrypt_cryptojs = void 0;
const CryptoJS = __importStar(require("crypto-js"));
//import pool from '../database';
class Encrypt_cryptojs {
    encrypt(data, password) {
        return __awaiter(this, void 0, void 0, function* () {
            const salt = CryptoJS.lib.WordArray.create([1123456789, 1123456789, 1123456789, 1123456789]);
            console.log(salt);
            // generate password based key
            const key = CryptoJS.PBKDF2(password, salt, {
                keySize: 8,
                iterations: 1024,
            });
            // encrypt using random IV
            const iv = CryptoJS.lib.WordArray.create([1123456789, 1123456789, 1123456789, 1123456789]);
            const encrypted = CryptoJS.AES.encrypt(data, key, {
                iv: iv,
                padding: CryptoJS.pad.Pkcs7,
                mode: CryptoJS.mode.CBC,
            });
            // salt (16 bytes) + iv (16 bytes)
            // prepend them to the ciphertext for use in decryption
            return salt.toString() + iv.toString() + encrypted.toString();
        });
    }
}
exports.encrypt_cryptojs = new Encrypt_cryptojs();
