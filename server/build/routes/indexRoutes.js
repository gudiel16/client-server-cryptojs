"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const c_usuario_1 = require("../controllers/c_usuario");
class IndexRoutes {
    constructor() {
        this.router = express_1.Router();
        this.config();
    }
    config() {
        this.router.post('/login', c_usuario_1.c_usuario.encrypt);
        this.router.post('/decrypt', c_usuario_1.c_usuario.decrypt);
    }
}
const indexRoutes = new IndexRoutes();
exports.default = indexRoutes.router;
