"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.c_usuario = void 0;
const encrypt_cryptojs_1 = require("../cifrar/encrypt_cryptojs");
const decrypt_cryptojs_1 = require("../cifrar/decrypt_cryptojs");
//import pool from '../database';
class C_usuario {
    encrypt(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const { nick, pass } = req.body;
            let cif = yield encrypt_cryptojs_1.encrypt_cryptojs.encrypt(pass, nick);
            res.status(200).json({ pass: cif, nick: nick });
        });
    }
    decrypt(req, res) {
        return __awaiter(this, void 0, void 0, function* () {
            const { data, nick } = req.body;
            let dec = yield decrypt_cryptojs_1.decrypt_cryptojs.decrypt(data, nick);
            res.status(200).send(dec);
        });
    }
}
exports.c_usuario = new C_usuario();
