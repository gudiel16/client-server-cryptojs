import { Component, OnInit } from '@angular/core';

//importamos servicio
import { ServiceLoginService } from '../../services/service-login.service';

import * as CryptoJS from 'crypto-js';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  constructor(private serviceLogin:ServiceLoginService) { }

  ngOnInit(): void {
  }

   enviar(){
    this.serviceLogin.login("miNickname","miPassword").subscribe((res:any) =>{

      let data = res['pass'];
      let nick = res['nick'];

      let dec = this.decryptData(data,nick);
      console.log(dec);

    });
  }

  //data es lo que esta encriptado
  decryptData(data:any, nick:any){
    
    const salt = CryptoJS.enc.Hex.parse(data.substr(0, 32));
    const iv = CryptoJS.enc.Hex.parse(data.substr(32, 32));
    const encrypted = data.substring(64);

    // generate password based key
    const key = CryptoJS.PBKDF2(nick, salt, {
        keySize: 8,
        iterations: 1024,
    });

    // decrypt using custom IV
    const decrypted = CryptoJS.AES.decrypt(encrypted, key, {
        iv: iv,
        padding: CryptoJS.pad.Pkcs7,
        mode: CryptoJS.mode.CBC,
    });
    
    console.log(data);
    console.log(nick);

    return decrypted.toString(CryptoJS.enc.Utf8);

  }

}
