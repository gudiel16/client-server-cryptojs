import { Injectable } from '@angular/core';

//para poder pedir datos (httpClient me va permitir poder hacer peticiones)
import { HttpClient, HttpHeaders } from '@angular/common/http';

//importamos para tener acceso a las rutas
import { Router } from '@angular/router';

//import { isNullOrUndefined } from 'util';
import { map } from 'rxjs/operators'
import { pipe,Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ServiceLoginService {

  API_URI='http://localhost:3000'

  constructor(private http:HttpClient, private router:Router) { }

  //para que headers sean json
  headers:HttpHeaders=new HttpHeaders({
    "Context-Type":"application/json"
  });

  login(nick:string,pass:string){
    return this.http.post(`${this.API_URI}/login`,
    {
      "nick":nick,
      "pass":pass
    },
    {headers:this.headers})
    .pipe(map(data=>data));    
  }

}
