# CRYPTO JS

Es una Biblioteca JavaScript de estándares de cifrado.

App realizada con Node & Typescript del lado del backend y Angular para Frontend.

### EJECUTAR
#### Backend:
```sh
cd server
npm install
```
Este comando hará  que se quede escuchando, para ir pasando todo lo de Typescript a JS:
```sh
npm run build
```
En otra consola ejecutar:
```sh
npm start
```

#### Frontend:
```sh
cd client
npm install
ng serve
```

